<?php

namespace Drupal\sightengine;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Drupal\sightengine\Plugin\Validation\Constraint\SightengineImageValidator;
use Drupal\sightengine\Plugin\Validation\Constraint\SightengineVideoValidator;

/**
 * Class SightengineManager.
 * Provides functions for sightengine.
 * @package Drupal\sightengine
 */
class SightengineManager {

  /**
   * The current user injected into the service.
   *
   * @var array
   */
  protected $config;

  /**
   * The current user injected into the service.
   *
   * @var array
   */
  protected $imageValidator;

  /**
   * The current user injected into the service.
   *
   * @var array
   */
  protected $videoValidator;

  /**
   * Constructs a new SightengineManager object.
   */
  public function __construct(SightengineImageValidator $image, SightengineVideoValidator $video) {
    $this->config = \Drupal::config('sightengine.settings');
    $this->http_client = \Drupal::httpClient();
    $this->imageValidator = $image;
    $this->videoValidator = $video;
  }

  public function getConfiguration() {
    return $this->config;
  }

  public function addConstraintForFields($entity_type, $bundle, &$fields) {
    if (!$this->config->isNew()) {
      $sightengine_field_config = $this->config->get('fields');
      $entity_type_name = $entity_type->id();
      if (!empty($sightengine_field_config) and array_key_exists($entity_type_name, $sightengine_field_config)) {
        if (array_key_exists($bundle, $sightengine_field_config[$entity_type_name])) {
          foreach ($sightengine_field_config[$entity_type_name][$bundle] as $field => $val) {
            if ($val != 0 and isset($fields[$field])) {
              $field_type = $fields[$field]->get('field_type');
              switch ($this->getValidationType($field_type)) {
                case 'text':
                case 'string':
                  $fields[$field]->addConstraint('sightengine_text', []);
                  break;
                case 'image':
                  $fields[$field]->addConstraint('sightengine_image', []);
                  break;
                case 'entity_reference':
                case 'file':
                  $fields[$field]->addConstraint('sightengine_file', []);
                  break;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Get type of validation.
   *
   * @param $str
   *   The field type id
   *
   * @return mixed
   */
  public function getValidationType($str) {
    $exps = [
      'text' => "/^[a-z_]{0,20}text[_a-z]{0,20}/",
      'string' => "/^[a-z_]{0,20}string[_a-z]{0,20}/",
      'image' => "/^[a-z_]{0,20}image[_a-z]{0,20}/",
      'file' => "/^[a-z_]{0,20}file[_a-z]{0,20}/",
      'entity_reference' => "/entity_reference/"
    ];
    foreach ($exps as $type => $exp) {
      if (preg_match($exp, $str)) {
        return $type;
      }
    }
    return $str;
  }

  /**
   * Get Response form sightengine.com.
   *
   * @param string $url
   *   The url of sightengine
   * @param array $params
   *   Params for request
   *
   * @return array
   */
  public function getValidateResponse($url, $params) {
    try {
      $request = \Drupal::httpClient()->post($url, $params);
      $response = Json::decode($request->getBody()->getContents());
      return $response;
    }
    catch (ClientException $e) {
      $response = Json::decode($e->getResponse()->getBody()->getContents());
      return $response;
    }
    catch (RequestException $e) {
      $response['error']['message'] = $e->getHandlerContext()['error'];
      $response['status'] = 'failure';
      return $response;
    }
  }

  public function getVideoModeration($file_uri, $context) {
    $this->videoValidator->getValidationResult($file_uri, $context);
  }

  public function getImageModeration($file_uri, $context) {
    $this->imageValidator->getValidationResult($file_uri, $context);
  }

}
