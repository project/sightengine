<?php

namespace Drupal\sightengine\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\file\Entity\File;

/**
 * Validates the sexual constraint.
 */
class SightengineVideoValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $seManager = \Drupal::service('sightengine');
    if (!empty($items->getValue())) {
      $fid = (int) $items->getValue()[0]['target_id'];
      $file = File::load($fid);
      $file_uri = $file->getFileUri();
      $this->getValidationResult($file_uri, $this->context);
    }
  }

  public function getRequestParams($config, $value) {
    $models = $config->get('models.video');
    $form_params = [
      'api_user' => $config->get('client_id'),
      'api_secret' => $config->get('client_secret'),
    ];
    $form_params['media'] = new \CurlFile($value);
    $form_params['models'] = $this->getModels($models);
    $request_params = [
      'timeout' => 10,
      'form_params' => $form_params,
      'headers' => [
        'Accept' => '*/*',
        'Content-type' => 'multipart/form-data',
      ],
      'curl' => [
        CURLOPT_POSTFIELDS => $form_params,
        CURLOPT_TIMEOUT => 500
      ],
    ];
    return $request_params;
  }

  public function getValidationResult($value, $context) {
    $service = \Drupal::service('sightengine');
    $config = $service->getConfiguration();
    $params = $this->getRequestParams($config, $value);
    // dd($config->get('validator_url.video'),$params);
    $response = $service->getValidateResponse($config->get('validator_url.video'), $params);
    $result = [
      "issue" => 0,
      "msg" => "Your video contains ",
    ];
    if ($response['status'] == 'success') {
      foreach ($response['data']['frames'] as $frame) {
        foreach($frame as $model => $info){
          if(!is_array($value)){
            if($value > 0.5){
              $result['msg'] .= '`' . $model . '` ';
              $result['issue'] += 1;
            }
          }
          else{
            if(isset($value['raw']) and $value['raw'] > 0.5){
              $result['msg'] .= '`' . $model . '` ';
              $result['issue'] += 1;
            }
            elseif(isset($value['prod']) and $value['prod'] > 0.5){
              $result['msg'] .= '`' . $model . '` ';
              $result['issue'] += 1;
            }
          }
        }
      }
    }
    else {
      $result['issue'] += 1;
      $result['msg'] = 'Can not validate because ' . $response['error']['message'];
    }
    if ($result['issue'] > 0) {
      $context->addViolation($result['msg']);
    }
  }

  protected function getModels($config) {
    $result = [];
    foreach ($config as $val) {
      if ($val != 0) {
        $result[] = $val;
      }
    }
    return implode(",", $result);
  }

}
