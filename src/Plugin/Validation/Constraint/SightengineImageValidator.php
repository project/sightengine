<?php

namespace Drupal\sightengine\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\file\Entity\File;

/**
 * Validates the sexual constraint.
 */
class SightengineImageValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    $seManager = \Drupal::service('sightengine');
    if (!empty($items->getValue())) {
      foreach ($items->getValue() as $item) {
        $fid = (int) $item['target_id'];
        $file = File::load($fid);
        $file_uri = $file->getFileUri();
        $this->getValidationResult($file_uri, $this->context);
      }
    }
  }

  public function getRequestParams($config, $value) {
    $models = $config->get('models.image');
    $form_params = [
      'api_user' => $config->get('client_id'),
      'api_secret' => $config->get('client_secret'),
    ];
    $form_params['media'] = new \CurlFile($value);
    $form_params['models'] = $this->getModels($models);
    $request_params = [
      'timeout' => 10,
      'form_params' => $form_params,
      'headers' => [
        'Accept' => '*/*',
        'Content-type' => 'multipart/form-data',
      ],
      'curl' => [CURLOPT_POSTFIELDS => $form_params],
    ];
    return $request_params;
  }

  public function getValidationResult($value, $context) {
    $service = \Drupal::service('sightengine');
    $config = $service->getConfiguration();
    $params = $this->getRequestParams($config, $value);
    $response = $service->getValidateResponse($config->get('validator_url.image'), $params);
    $result = [
      "issue" => 0,
      "msg" => "Your image contains ",
    ];
    if ($response['status'] == 'success') {
      foreach (array_slice($response, 2, count($response) - 3) as $key => $val) {
        if ($key === 'nudity') {
          if ($val['raw'] > 0.5) {
            $result['msg'] .= '`' . $key . '` ';
            $result['issue'] += 1;
          }
        }
        elseif ($key === 'gore') {
          if ($val['prob'] > 0.5) {
            $result['msg'] .= '`' . $key . '` ';
            $result['issue'] += 1;
          }
        }
        else {
          if ($val > 0.5) {
            $result['msg'] .= '`' . $key . '` ';
            $result['issue'] += 1;
          }
        }
      }
    }
    else {
      $result['issue'] += 1;
      $result['msg'] = 'Can not validate because ' . $response['error']['message'];
    }
    if ($result['issue'] > 0) {
      $context->addViolation($result['msg']);
    }
  }

  protected function getModels($config) {
    $result = [];
    foreach ($config as $val) {
      if ($val != 0) {
        $result[] = $val;
      }
    }
    return implode(",", $result);
  }

}
