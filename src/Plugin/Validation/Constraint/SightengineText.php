<?php

namespace Drupal\sightengine\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "sightengine_text",
 *   label = @Translation("SightEngine for text", context = "Validation"),
 *   type = "string"
 * )
 */
class SightengineText extends Constraint {

  public function getMessage($errors) {
    return "Error message";
  }

}
