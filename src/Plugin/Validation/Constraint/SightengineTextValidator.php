<?php

namespace Drupal\sightengine\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the sexual constraint.
 */
class SightengineTextValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      $this->getValidationResult($item->value, $this->context);
    }
  }

  /**
   * Get parameters for validate request.
   *
   * @param $config
   *   Entity config.
   * @param $value
   *   The value need validate
   *
   * @return array
   */
  public function getRequestParams($config, $value) {
    $form_params = [
      'api_user' => $config->get('client_id'),
      'api_secret' => $config->get('client_secret'),
    ];
    $form_params['text'] = $value;
    $form_params['mode'] = $config->get('mode');
    $form_params['opt_countries'] = $config->get('opt_countries');
    $form_params['lang'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $request_params = [
      'timeout' => 100,
      'form_params' => $form_params,
      'headers' => [
        'Accept' => 'application/json',
      ],
    ];
    return $request_params;
  }

  public function getValidationResult($value, $context) {
    $service = \Drupal::service('sightengine');
    $config = $service->getConfiguration();
    $params = $this->getRequestParams($config, $value);
    $response = $service->getValidateResponse($config->get('validator_url.text'), $params);
    $result = [
      "issue" => 0,
      "msg" => "Your post contains ",
    ];
    if ($response['status'] == 'success') {
      foreach (array_slice($response, 2) as $key => $val) {
        if (!empty($val['matches'])) {
          foreach ($val['matches'] as $m) {
            if ($this->checkIgnoreModels($m['type'], $key, $config)) {
              $result['msg'] .= '`' . $m['type'] . '` ';
              $result['issue'] += 1;
            }
          }
        }
      }
    }
    else {
      $result['issue'] += 1;
      $result['msg'] = 'Can not validate because ' . $response['error']['message'];
    }
    if ($result['issue'] > 0) {
      $context->addViolation($result['msg']);
    }
  }

  protected function checkIgnoreModels($type, $key, $config) {
    $ignore_models = $config->get('models.text_ignore');
    if (preg_match('/^phone_number_[A-Za-z]+/', $type)) {
      if (!in_array('phone_number', $ignore_models[$key])) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      if (!in_array($type, $ignore_models[$key])) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

}
