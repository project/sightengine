<?php

namespace Drupal\sightengine\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "sightengine_file",
 *   label = @Translation("SightEngine for video", context = "Validation"),
 *   type = "string"
 * )
 */
class SightengineFile extends Constraint {

  public $issue = 'Your content contains %value elements';

  public function getMessage($errors) {
    return "Error message";
  }

}
