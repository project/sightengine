<?php

namespace Drupal\sightengine\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;


/**
 * Validates the sexual constraint.
 */
class SightengineFileValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!empty($items->getValue())) {
      $field_type = $items->getFieldDefinition()->get('field_type');

      if($field_type == 'file'){
        foreach ($items->getValue() as $item) {
          $fid = (int) $item['target_id'];
          $this->getFileModeration($fid);
        }
      }
      else{
        foreach ($items->getValue() as $item) {
          $mid = (int) $item['target_id'];
          $media = Media::load($mid);
          if (is_null($media)) {
            return;
          }
          if ($media->bundle() == 'image' || $media->bundle() == 'video'){
            $fields = $media->getFields();
            $files = array_pop($fields)->getValue();
            foreach($files as $file);
            $fid = (int) $file['target_id']; 
            $this->getFileModeration($fid);
          }
        }
      }
    }
  }

  public function getFileType($str) {
    $exps = [
      'image' => "/^image\/[A-Za-z0-9]+/",
      'video' => "/^video\/[A-Za-z0-9]+/",
    ];
    foreach ($exps as $type => $exp) {
      if (preg_match($exp, $str)) {
        return $type;
      }
    }
    return $str;
  }

  protected function getFileModeration($fid){
    $seManager = \Drupal::service('sightengine');
    $file = File::load($fid);
    $file_uri = $file->getFileUri();
    $file_type = $this->getFileType($file->getMimeType());
    if ($file_type === 'video') {
      $seManager->getVideoModeration($file_uri, $this->context);
    }
    elseif ($file_type === 'image') {
      $seManager->getImageModeration($file_uri, $this->context);
    }
  }
}
