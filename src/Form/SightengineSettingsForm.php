<?php

namespace Drupal\sightengine\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define Sightengine Settings Form.
 *
 * @package Drupal\sightengine\Form
 */
class SightengineSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sightengine_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $sightengine_config = $this->config('sightengine.settings');
    $image_models = $sightengine_config->get('models.image');
    $video_models = $sightengine_config->get('models.video');
    $text_ignore_models = $sightengine_config->get('models.text_ignore');
    $form['app'] = [
      '#type' => 'details',
      '#title' => $this->t('App informations'),
      '#open' => TRUE,
    ];
    $form['app'][] = [
      'client_id' => [
        '#type' => 'textfield',
        '#title' => $this->t('Client ID'),
        '#default_value' => $sightengine_config->get('client_id'),
        '#required' => TRUE,
      ],
      'client_secret' => [
        '#type' => 'textfield',
        '#title' => $this->t('Client Secret'),
        '#default_value' => $sightengine_config->get('client_secret'),
        '#required' => TRUE,
      ],
    ];
    $form['text'] = [
      '#type' => 'details',
      '#title' => $this->t('Text moderation'),
      '#open' => TRUE,
    ];
    $form['text']['text_validator_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Text validator URL'),
      '#default_value' => $sightengine_config->get('validator_url.text'),
      '#required' => TRUE,
    ];
    $form['text'][] = [
      'opt_country' => [
        '#type' => 'textfield',
        '#title' => $this->t('OPT country'),
        '#default_value' => $sightengine_config->get('opt_country'),
        '#required' => TRUE,
        '#description' => 'The UTF-8 formatted text along with the ISO 639-1 language code (such as en for english) and the comma-separated list of countries for phone number detection (such as us,gb,fr for the United States, United Kingdom and France).',
      ],
      'mode' => [
        '#type' => 'radios',
        '#title' => $this->t('Mode'),
        '#default_value' => ($sightengine_config->get('mode') != NULL) ? $sightengine_config->get('mode') : 'standard',
        '#options' => [
          'standard' => $this->t('Standard'),
          'username' => $this->t('Username'),
        ],
      ],
    ];
    $form['text']['ignore'] = [
      '#type' => 'details',
      '#title' => $this->t('Ignore models'),
      '#open' => FALSE,
    ];
    $form['text']['ignore'][] = [
      'ignore_models_profanity' => [
        "#type" => 'checkboxes',
        '#title' => 'Profanity',
        '#default_value' => (isset($text_ignore_models['profanity'])) ? $text_ignore_models['profanity'] : [],
        '#options' => [
          'sexual' => $this->t('Sexual content'),
          'discriminatory' => $this->t('Discriminatory and derogatory content'),
          'insult' => $this->t('Insults'),
          'inappropriate' => $this->t('Inappropriate language'),
          'other_profanity' => $this->t('Other types of profanity'),
        ],
      ],
      'ignore_models_personal' => [
        "#type" => 'checkboxes',
        '#title' => 'Personal',
        '#default_value' => (isset($text_ignore_models['personal'])) ? $text_ignore_models['personal'] : [],
        '#options' => [
          'email' => $this->t('Email addresses'),
          'phone_number' => $this->t('Phone numbers,'),
          'ipv4' => $this->t('IPs (version 4)'),
          'ipv6' => $this->t('IPs (version 6)'),
        ],
      ],
      'ignore_models_link' => [
        "#type" => 'checkboxes',
        '#title' => 'Link',
        '#default_value' => (isset($text_ignore_models['link'])) ? $text_ignore_models['link'] : [],
        '#options' => [
          'url' => $this->t('URLs'),
        ],
      ],
    ];
    // Image moderation form
    $form['image'] = [
      '#type' => 'details',
      '#title' => $this->t('Image moderation'),
      '#open' => FALSE,
    ];
    $form['image'][] = [
      'image_validator_url' => [
        '#type' => 'url',
        '#title' => $this->t('Image validator URL'),
        '#default_value' => $sightengine_config->get('validator_url.image'),
        '#required' => FALSE,
      ],
      'image_models' => [
        "#type" => 'checkboxes',
        '#title' => 'Models',
        '#default_value' => ($image_models != NULL) ? $image_models : ['nudity'],
        '#options' => [
          'nudity' => $this->t('Nudity detection'),
          'wad' => $this->t('Weapons, alcohol & drugs'),
          'gore' => $this->t('Graphic violence & gore'),
          'offensive' => $this->t('Offensive'),
        ],
      ],
    ];
    $form['video'] = [
      '#type' => 'details',
      '#title' => $this->t('Video moderation'),
      '#open' => FALSE,
      "#access" => TRUE,
    ];
    $form['video'][] = [
      'video_validator_url' => [
        '#type' => 'url',
        '#title' => $this->t('Video validator URL'),
        '#default_value' => $sightengine_config->get('validator_url.video'),
        '#required' => FALSE,
      ],
      'video_models' => [
        "#type" => 'checkboxes',
        '#title' => 'Models',
        '#default_value' => ($video_models != NULL) ? $video_models : ['nudity'],
        '#options' => [
          'nudity' => $this->t('Nudity detection'),
          'wad' => $this->t('Weapons, alcohol & drugs'),
          'gore' => $this->t('Graphic violence & gore'),
          'offensive' => $this->t('Offensive'),
        ],
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('sightengine.settings')
      ->set('validator_url.text', $values['text_validator_url'])
      ->set('validator_url.image', $values['image_validator_url'])
      ->set('validator_url.video', $values['video_validator_url'])
      ->set('client_id', $values['client_id'])
      ->set('client_secret', $values['client_secret'])
      ->set('opt_country', $values['opt_country'])
      ->set('mode', $values['mode'])
      ->set('models.image', $values['image_models'])
      ->set('models.video', $values['video_models'])
      ->set('models.text_ignore.profanity', $values['ignore_models_profanity'])
      ->set('models.text_ignore.personal', $values['ignore_models_personal'])
      ->set('models.text_ignore.link', $values['ignore_models_link'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sightengine.settings'];
  }

}
